﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using MediaPortalApi.Code;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace MediaPortalApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly MediaRepository _mediaRepository;

        public MediaController(IConfiguration configuration)
        {
            _configuration = configuration;
            string dataFolder = _configuration.GetSection("AppSettings").GetSection("DataFolder").Value;
            _mediaRepository = new MediaRepository(dataFolder);
        }
        
        [HttpGet("media")]
        public IEnumerable<MediaItem> GetMediaItems()
        {
            return _mediaRepository.GetMediaItems();
        }

        [HttpGet("media/{id}")]
        public MediaItem GetMediaItem(int id)
        {
            return _mediaRepository.GetMediaItem(id);
        }

        [HttpGet("categories")]
        public IEnumerable<Category> GetCategories()
        {
            return _mediaRepository.GetCategories();
        }
        

        // GET: api/Media/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Media
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Media/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
