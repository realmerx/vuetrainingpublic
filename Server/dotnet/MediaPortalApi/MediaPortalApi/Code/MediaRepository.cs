﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace MediaPortalApi.Code
{
    public class MediaRepository
    {
        private string _dataFolder;
        private IEnumerable<MediaItem> _mediaItems;
        private IEnumerable<Category> _categories;
        
        public MediaRepository(string dataFolder)
        {
            _dataFolder = dataFolder;
            _mediaItems = GetMediaItemsFromJson();
            _categories = GetCategoriesFromJson();
        }

        private IEnumerable<MediaItem> GetMediaItemsFromJson()
        {
            string jsonData = System.IO.File.ReadAllText(_dataFolder + "medias.json");
            return JsonSerializer.Deserialize<IEnumerable<MediaItem>>(jsonData, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        }

        private IEnumerable<Category> GetCategoriesFromJson()
        {
            string jsonData = System.IO.File.ReadAllText(_dataFolder + "categories.json");
            return JsonSerializer.Deserialize<IEnumerable<Category>>(jsonData, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        }

        internal MediaItem GetMediaItem(int id)
        {
            return _mediaItems.First(item => item.Id == id);
        }

        public IEnumerable<MediaItem> GetMediaItems()
        {
            return _mediaItems;
        }

        public IEnumerable<Category> GetCategories()
        {
            return _categories;
        }
    }
}
